<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// User
Route::post('/user/register', 'AuthController@register');
Route::post('/user/login', 'AuthController@login');
Route::post('/user/uploadFotoUser', 'AuthController@uploadFotoUser')->middleware('auth:api');
// Route::delete('/user/delete/{user_id}', 'AuthController@delete');
Route::get('/user/userDetail', 'AuthController@userDetail')->middleware('auth:api');
Route::post('/user/update', 'AuthController@update')->middleware('auth:api');
Route::get('/user/progressBar', 'AuthController@progressBar')->middleware('auth:api');
Route::post('/user/createKos', 'AuthController@createKos')->middleware('auth:api', 'owner_kos');
Route::get('/user/getUserById/{id_user}', 'AuthController@getUserById')->middleware('auth:api');
Route::get('/user/profilProgressBar/{id_user}', 'AuthController@profilProgressBar')->middleware('auth:api');

// CV
Route::get('/cv/index', 'CvController@index')->middleware('auth:api');
Route::get('/cv/view', 'CvController@view')->middleware('auth:api');
Route::post('/cv/create', 'CvController@create')->middleware('auth:api');
Route::post('/cv/update', 'CvController@update')->middleware('auth:api');
Route::delete('/cv/delete', 'CvController@delete')->middleware('auth:api');

// SkillDanKemampuan
Route::get('/skill/index', 'SkillDanKemampuanController@index')->middleware('auth:api');
Route::post('/skill/add', 'SkillDanKemampuanController@add')->middleware('auth:api');
Route::delete('/skill/delete/{id}', 'SkillDanKemampuanController@delete')->middleware('auth:api');
Route::post('/skill/massInsert', 'SkillDanKemampuanController@massInsert')->middleware('auth:api');

// Pengalaman Kerja
Route::get('/pengalaman_kerja/index', 'PengalamanKerjaController@index')->middleware('auth:api');
Route::post('/pengalaman_kerja/create', 'PengalamanKerjaController@create')->middleware('auth:api');
Route::delete('/pengalaman_kerja/delete/{id}', 'PengalamanKerjaController@delete')->middleware('auth:api');
Route::post('/pengalaman_kerja/massInsert', 'PengalamanKerjaController@massInsert')->middleware('auth:api');

// Notification
Route::get('/notification/index', 'NotificationController@index')->middleware('auth:api', 'anak_kos');
Route::get('/notification/indexByOwner', 'NotificationController@indexByOwner')->middleware('auth:api', 'owner_kos');
Route::get('/notification/notifikasiLengkapiData/{id_anak_kos}', 'NotificationController@notifikasiLengkapiData')->middleware('auth:api', 'owner_kos');
Route::get('/notification/changeStatusToInactive/{id}', 'NotificationController@changeStatusToInactive')->middleware('auth:api');
Route::get('/notification/makeSurvey/{id_kos}', 'NotificationController@makeSurvey')->middleware('auth:api', 'anak_kos');
