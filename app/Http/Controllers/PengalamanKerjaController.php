<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PengalamanKerja;
use App\Models\Cv;
use Auth;
use App\Transformers\PengalamanKerjaTransformer;

class PengalamanKerjaController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $pengalaman_kerja = PengalamanKerja::where('cv_id', $user->cv->id)->get();

        return fractal()
          ->collection($pengalaman_kerja)
          ->transformWith(new PengalamanKerjaTransformer)
          ->toArray();
    }

    public function create(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'pengalaman_kerja'  => 'required',
        ]);

        $pengalaman_kerja = PengalamanKerja::create([
            'cv_id'             => $user->cv->id,
            'pengalaman_kerja'  => $request->pengalaman_kerja,
            'mulai'             => $request->mulai,
            'selesai'           => $request->selesai,
        ]);

        return fractal($pengalaman_kerja, new PengalamanKerjaTransformer)->toArray();
    }

    public function delete($id)
    {
        $pengalaman_kerja = PengalamanKerja::find($id);
        if($pengalaman_kerja == null){
            return response()->json(['message' => 'pengalaman kerja tidak ditemukan'], 422);
        }
        $pengalaman_kerja->delete();
        return response()->json(['delete' => 'success'], 202);
    }

    public function massInsert(Request $request){
        // $cv_id = $request->data[0]['cv_id'];
        $user = Auth::user();
        $cv = Cv::where('user_id', $user->id)->first();
        $id_all = [];
        foreach ($request->data as $data) {
            if($data['id'] != null){
                $pengalaman_kerja = PengalamanKerja::find($data['id']);
                if($pengalaman_kerja->pengalaman_kerja != $data['pengalaman_kerja']){
                    $pengalaman_kerja->pengalaman_kerja = $data['pengalaman_kerja'];
                    $pengalaman_kerja->save();
                    $id_all[] = $pengalaman_kerja->id;
                }
                if($pengalaman_kerja->mulai != $data['mulai']){
                    $pengalaman_kerja->mulai = $data['mulai'];
                    $pengalaman_kerja->save();
                    $id_all[] = $pengalaman_kerja->id;
                }
                if($pengalaman_kerja->selesai != $data['selesai']){
                    $pengalaman_kerja->selesai = $data['selesai'];
                    $pengalaman_kerja->save();
                    $id_all[] = $pengalaman_kerja->id;
                }
                $id_all[] = $pengalaman_kerja->id;
            }elseif($data['id'] == null){
                $pengalaman_kerja = PengalamanKerja::create([
                    'cv_id'             => $cv->id,
                    'pengalaman_kerja'  => $data['pengalaman_kerja'],
                    'mulai'             => $data['mulai'],
                    'selesai'           => $data['selesai'],
                ]);
                $id_all[] = $pengalaman_kerja->id;
            }
        }

        $pengalaman_kerja_delete = PengalamanKerja::where('cv_id', $cv->id)->get();
        foreach ($pengalaman_kerja_delete as $delete) {
            if(!in_array($delete->id, $id_all)){
                $delete->delete();
            }
        }

        return redirect()->action('PengalamanKerjaController@index');
    }
}
