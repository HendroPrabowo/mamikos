<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cv;
use App\Transformers\CvTransformer;
use App\Transformers\CvCompleteTransformer;
use Auth;
use Illuminate\Support\Facades\Storage;

class CvController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $cv = $user->cv;
        return fractal($cv, new CvCompleteTransformer)->toArray();
    }

    public function view()
    {
        $user = Auth::user();

        $cv = Cv::where('user_id', $user->id)->first();

        return fractal($cv, new CvTransformer)->toArray();
    }

    public function update(Request $request)
    {
        $cv = Auth::user()->cv;

        $this->validate($request, [
            'penghasilan_terakhir'  => 'integer',
            'upload_cv'             => "mimes:doc,docx,pdf",
        ]);

        $cv->update([
          'pendidikan_terakhir'   => $request->pendidikan_terakhir,
          'penghasilan_terakhir'  => $request->penghasilan_terakhir,
          'gaji_min'              => $request->gaji_min,
          'gaji_max'              => $request->gaji_max,
        ]);

        // Penggantian cv sebelumnya
        if($request->upload_cv != null){
            $fileName = $cv->id . time() . '.' . $request->file('upload_cv')->getClientOriginalExtension();
            $path = $request->file('upload_cv')->storeAs('public/cv/', $fileName);
            $cv->update([
                'upload_cv' => 'storage/cv/' . $fileName,
            ]);
        }

        return fractal($cv, new CvTransformer)->toArray();
    }

    public function delete()
    {
        $cv = Auth::user()->cv;
        if($cv == null){
            return response()->json(['message' => 'cv tidak ditemukan'], 422);
        }

        // Hapus skill dan kemampuan
        $skill_dan_kemampuan = $cv->skills;
        foreach ($skill_dan_kemampuan as $key => $skill) {
            $skill->delete();
        }

        // Hapus pengalaman Kerja
        $pengalaman_kerja = $cv->pengalamanKerja;
        foreach ($pengalaman_kerja as $key => $pengalaman) {
            $pengalaman->delete();
        }

        // Hapus cv
        $cv->delete();
        return response()->json(['delete' => 'success'], 202);
    }
}
