<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SkillDanKemampuan;
use App\Models\Cv;
use App\Transformers\SkillDanKemampuanTransformer;
use Auth;

class SkillDanKemampuanController extends Controller
{
    // get user skill dan kemampuan
    public function index()
    {
        $user = Auth::user();
        $skill = SkillDanKemampuan::where('cv_id', $user->cv->id)->get();
        return fractal()
          ->collection($skill)
          ->transformWith(new SkillDanKemampuanTransformer)
          ->toArray();
    }

    // add user skill dan kemampuan
    public function add(Request $request)
    {
        $user = auth::user();

        $skill = SkillDanKemampuan::create([
            'cv_id'   => $user->cv->id,
            'skill'   => $request->skill,
        ]);

        return fractal($skill, new SkillDanKemampuanTransformer)->toArray();
    }

    // delete skill dan kemampuan
    public function delete($id)
    {
        $skill_dan_kemampuan = SkillDanKemampuan::find($id);
        if($skill_dan_kemampuan == null){
            return response()->json(['message' => 'skill dan kemampuan tidak ditemukan'], 422);
        }
        $skill_dan_kemampuan->delete();
        return response()->json(['delete' => 'success'], 202);
    }

    public function massInsert(Request $request)
    {
        $user = Auth::user();
        $cv = Cv::where('user_id', $user->id)->first();
        $id_all = []; // create variable id
        foreach ($request->data as $data) {
            if($data['id'] != null){
                $skill_dan_kemampuan = SkillDanKemampuan::find($data['id']);
                // dd($skill_dan_kemampuan);
                // if($skill_dan_kemampuan->skill != $data['skill']){
                    $skill_dan_kemampuan->skill = $data['skill'];
                    $skill_dan_kemampuan->save();
                    $id_all[] = $skill_dan_kemampuan->id;
                // }
            }elseif ($data['id'] == null) {
                $a = SkillDanKemampuan::create([
                    'cv_id' => $cv->id,
                    'skill'   => $data['skill'],
                ]);
                $id_all[] = $a->id;
            }
        }
        // dd($id_all);
        $skill_dan_kemampuan_delete = SkillDanKemampuan::where('cv_id', $cv->id)->get();
        foreach($skill_dan_kemampuan_delete as $delete){
            if(!in_array($delete->id, $id_all)){
                $delete->delete();
            }
        }

        return redirect()->action('SkillDanKemampuanController@index');
    }
}
