<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Cv;
use App\Models\SkillDanKemampuan;
use App\Models\PengalamanKerja;
use App\Models\Kos;
use App\Models\Notification;
use App\Models\ProfesiKuliah;
use App\Models\ProfesiBekerja;
use App\Models\ProfesiLainnya;
use App\Transformers\CvTransformer;
use App\Transformers\UserTransformer;
use App\Transformers\SkillDanKemampuanTransformer;
use App\Transformers\ProfesiKuliahTransformer;
use App\Transformers\ProfesiBekerjaTransformer;
use Illuminate\Support\Facades\Storage;
use Auth;
use Config;

class AuthController extends Controller
{
    // Register user baru sebagai anak kost
    public function register(Request $request)
    {
        $this->validate($request, [
            'nama_depan'            => 'required',
            'nama_belakang'         => 'required',
            'email'                 => 'required|email|unique:users',
            'telepon'               => 'required|unique:users',
            'password'              => 'required|confirmed',
            'password_confirmation' => 'required',
            'profesi_id'            => 'required',
        ]);

        $user = User::create([
            'nama_depan'          => $request->nama_depan,
            'nama_belakang'       => $request->nama_belakang,
            'email'               => $request->email,
            'jenis_kelamin'       => $request->jenis_kelamin,
            'tanggal_lahir'       => $request->tanggal_lahir,
            'kota_asal'           => $request->kota_asal,
            'profesi_id'          => $request->profesi_id,
            'telepon'             => $request->telepon,
            'alamat'              => $request->alamat,
            'password'            => bcrypt($request->password),
            'api_token'           => bcrypt($request->email),
        ]);

        if($request->profesi_id == 1){
            $profesi_kuliah = ProfesiKuliah::create([
                'user_id'       => $user->id,
                'jenis_kuliah'  => $request->jenis_kuliah,
                'universitas'   => $request->universitas,
                'jurusan'       => $request->jurusan,
                'semester'      => $request->semester,
                'profesi'       => $request->profesi,
                'field'         => $request->field,
            ]);
        }elseif ($request->profesi_id == 2) {
            $profesi_bekerja = ProfesiBekerja::create([
                'user_id'       => $user->id,
                'jenis_bekerja' => $request->jenis_bekerja,
                'kantor'        => $request->kantor,
                'posisi'        => $request->posisi,
            ]);
        }elseif ($request->profesi_id == 3) {
            $profesi_lainnya = ProfesiLainnya::create([
                'user_id'     => $user->id,
                'keterangan'  => $request->keterangan,
            ]);
        }

        if($request->foto != null){
            $imageName = $user->id . time() . '.' . $request->file('foto')->getClientOriginalExtension();
            $path = $request->file('foto')->storeAs('public/foto_profil/', $imageName);
            $user->update([
                'foto'  => 'storage/foto_profil/' . $imageName,
            ]);
        }

        $data = User::find($user->id);

        // Langsung buat cv
        Cv::create([
            'user_id' => $user->id,
        ]);

        return fractal($data, new UserTransformer)->toArray();
    }

    // Login
    public function login(Request $request)
    {
        // Validasi input
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required',
        ]);

        // Cek login
        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return response()->json(['error' => 'Email of Password wrong'], 401);
        }

        return fractal(Auth::user(), new UserTransformer)->toArray();
    }

    // Menghapus account user
    public function delete($user_id)
    {
        User::destroy($user_id);

        return response()->json('Success');
    }

    public function userDetail()
    {
        $user = Auth::user();
        return fractal($user, new UserTransformer)->toArray();
    }

    // Update profil users
    public function update(Request $request)
    {
        $user = Auth::user();
        if($user->email != $request->email){
            $this->validate($request, [
                'email'   => 'required|email|unique:users',
            ]);
        }

        $this->validate($request, [
            'nama_depan'    => 'required',
            'nama_belakang' => 'required',
            'telepon'       => 'required',
            'profesi_id'    => 'required',
            'foto'          => 'image',
        ]);

        $user->update([
          'nama_depan'          => $request->nama_depan,
          'nama_belakang'       => $request->nama_belakang,
          'email'               => $request->email,
          'jenis_kelamin'       => $request->jenis_kelamin,
          'tanggal_lahir'       => $request->tanggal_lahir,
          'kota_asal'           => $request->kota_asal,
          'telepon'             => $request->telepon,
          'alamat'              => $request->alamat,
        ]);

        if($request->foto != null){
            $imageName = $user->id . time() . '.' . $request->file('foto')->getClientOriginalExtension();

            // Hapus dulu foto yang ada
            $path = $request->file('foto')->storeAs('public/foto_profil/', $imageName);
            $user->update([
                'foto'  => 'storage/foto_profil/' . $imageName,
            ]);
        }

        // <!------------ Kondisi jika profesi berubah  ------------------->
        // Hapus data profesi sebelumnya
        if($user->profesi_id != $request->profesi_id){
            if($user->profesi_id == 1){
                ProfesiKuliah::where('user_id', $user->id)->delete();
            }elseif ($user->profesi_id == 2) {
                ProfesiBekerja::where('user_id', $user->id)->delete();
            }elseif ($user->profesi_id == 3) {
                ProfesiLainnya::where('user_id', $user->id)->delete();
            }
        }
        // Buat data profesi baru di tabel profesi yg sesuai
        if($user->profesi_id != $request->profesi_id){
            if($request->profesi_id == 1){
                ProfesiKuliah::create([
                    'user_id' => $user->id,
                ]);
            }elseif ($request->profesi_id == 2) {
                ProfesiBekerja::create([
                    'user_id' => $user->id,
                ]);
            }elseif ($request->profesi_id == 3) {
                ProfesiLainnya::create([
                    'user_id' => $user->id,
                ]);
            }
        }
        // <-------------------------------------------------------------->

        // Ganti profesi_id users
        $user->profesi_id = $request->profesi_id;
        $user->save();

        if($user->profesi_id == 1){
            $profesi_kuliah               = ProfesiKuliah::where('user_id', $user->id)->first();
            $profesi_kuliah->jenis_kuliah = $request->jenis_kuliah;
            $profesi_kuliah->universitas  = $request->universitas;
            $profesi_kuliah->jurusan      = $request->jurusan;
            $profesi_kuliah->semester     = $request->semester;
            $profesi_kuliah->profesi      = $request->profesi;
            $profesi_kuliah->field        = $request->field;
            $profesi_kuliah->save();
        }elseif ($user->profesi_id == 2) {
            $profesi_bekerja                = ProfesiBekerja::where('user_id', $user->id)->first();
            $profesi_bekerja->jenis_bekerja = $request->jenis_bekerja;
            $profesi_bekerja->kantor        = $request->kantor;
            $profesi_bekerja->posisi        = $request->posisi;
            $profesi_bekerja->save();
        }elseif ($user->profesi_id == 3) {
            $profesi_lainnya              = ProfesiLainnya::where('user_id', $user->id)->first();
            $profesi_lainnya->keterangan  = $request->keterangan;
            $profesi_lainnya->save();
        }
        return fractal($user, new UserTransformer)->toArray();
    }

    public function progressBar()
    {
        $user     = Auth::user();
        $counter  = 0;
        $total    = 0;
        $yang_harus_diisi = [];

        // Hitung yang kosong dari tabel user
        $user_array = fractal($user, new UserTransformer)->toArray();
        $key = ["foto", "nama_depan", "nama_belakang", "email", "jenis_kelamin", "tanggal_lahir", "kota_asal", "profesi_id", "telepon", "alamat"];
        for($i=0; $i<10; $i++){
            if($user_array['data'][$key[$i]] == null){
                $counter++;
                $yang_harus_diisi[] = $key[$i];
            }
            $total++;
        }

        // Hitung yang kosong jika profesi jenis_kuliah
        if($user->profesi_id == 1){
            $profesi_kuliah = ProfesiKuliah::where('user_id', $user->id)->first();
            $profesi_kuliah_array = fractal($profesi_kuliah, new ProfesiKuliahTransformer)->toArray();
            $key2 = ["jenis_kuliah", "universitas", "jurusan", "semester"];
            // dd($profesi_kuliah_array);
            for($i=0; $i<4; $i++){
                if($profesi_kuliah_array['data'][$key2[$i]] == null){
                    $counter++;
                    $yang_harus_diisi[] = $key2[$i];
                }
                $total++;
            }
            // Hitung yang kosong jika kuliah freelance
            if($profesi_kuliah->jenis_kuliah == 'freelance'){
                if($profesi_kuliah->profesi == null){
                    $counter++;
                    $yang_harus_diisi[] = "profesi";
                }
                $total++;
            }
            // Hitung yang kosong jika parttime
            if($profesi_kuliah->jenis_kuliah == 'parttime'){
                if($profesi_kuliah->profesi == null){
                    $counter++;
                    $yang_harus_diisi[] = "profesi";
                }
                if($profesi_kuliah->field == null){
                    $counter++;
                    $yang_harus_diisi[] = "field";
                }
                $total += 2;
            }
        }elseif ($user->profesi_id == 2) {  // Hitung yang kosong jika profesi bekerja
            $profesi_bekerja = ProfesiBekerja::where('user_id', $user->id)->first();
            if($profesi_bekerja->jenis_bekerja == null){
                $counter++;
                $total++;
                $yang_harus_diisi[] = "jenis_bekerja";
            }elseif ($profesi_bekerja->jenis_bekerja == 'onlineshop') {
                $total++;
            }else {
                $profesi_bekerja_array = fractal($profesi_bekerja, new ProfesiBekerjaTransformer)->toArray();
                $key3 = ["jenis_bekerja", "kantor", "posisi"];
                for($i=0; $i<3; $i++){
                    if($profesi_bekerja_array['data'][$key3[$i]] == null){
                        $counter++;
                        $yang_harus_diisi[] = $key3[$i];
                    }
                    $total++;
                }
            }
        }elseif ($user->profesi_id == 3) { // Hitung yang kosong jika profesi lainnya
            $profesi_lainnya = ProfesiLainnya::where('user_id', $user->id)->first();
            if(empty($profesi_lainnya->keterangan)){
                $counter++;
                $yang_harus_diisi[] = "keterangan";
            }
            $total++;
        }

        // Hitung dari cv
        $cv = Cv::where('user_id', $user->id)->first();
        $cv_array = fractal($cv, new CvTransformer)->toArray();
        $key4 = ["pendidikan_terakhir", "penghasilan_terakhir", "gaji_min", "gaji_max", "upload_cv"];
        foreach ($key4 as $key) {
            if($cv_array["data"][$key] == null){
                $counter++;
                $yang_harus_diisi[] = $key;
            }
            $total++;
        }

        // Apakah dia sudah mengisi skill dan kemampuan (Minimal 1)
        $skill_dan_kemampuan = SkillDanKemampuan::where('cv_id', $cv->id)->first();
        if($skill_dan_kemampuan == null){
            $counter++;
            $yang_harus_diisi[] = "skill_dan_kemampuan";
        }
        $total++;

        // Apakah dia sudah mengisi pengalaman kerja (Minimal 1)
        $pengalaman_kerja = PengalamanKerja::where('cv_id', $cv->id)->first();
        if($pengalaman_kerja == null){
            $counter++;
            $yang_harus_diisi[] = "pengalaman_kerja";
        }
        $total++;

        // $fill adalah variabel penampung jumlah yang telah diisi
        $fill = $total - $counter;

        if($fill == $total){
            $persentase = 100;
        }else{
            $persentase = ($fill/$total) * 100;
        }

        return response()->json([
            'kosong'      => $counter,
            'total'       => $total,
            'persentase'  => $persentase,
            'data'        => $yang_harus_diisi,
        ]);
    }

    public function createKos(Request $request)
    {
        $user = Auth::user();
        $kos = Kos::where('user_id', $user->id)->first();

        if(!empty($kos)){
            return response()->json(['message' => 'hanya bisa membuat kos sekali, anda sudah mempunyai kos'], 422);
        }

        $request->validate([
            'foto'      => 'image|required',
            'nama_kos'  => 'required',
            'kota'      => 'required',
        ]);

        $kos = Kos::create([
            'nama_kos'  => $request->nama_kos,
            'user_id'   => $user->id,
            'kota'      => $request->kota,
        ]);

        if(!empty($request->foto)){
            $imageName = $user->id . time() . '.' . $request->file('foto')->getClientOriginalExtension();
            $path = $request->file('foto')->storeAs('public/foto_kos', $imageName);
            $kos->update([
                'foto'  => 'storage/foto_kos/' . $imageName,
            ]);
        }

        // Notifikasi ke anak kos bahwa ada kos baru
        $anak_kos = User::where('role', 2)->get();
        foreach ($anak_kos as $anak) {
            Notification::create([
                'notification'  => 'Ada rekomendasi kos baru nih untuk kamu! Cek yuk, siapa tau cocok.',
                'user_id'       => $user->id,
                'anak_kos_id'   => $anak->id,
                'kos_id'        => $kos->id,
            ]);
        }

        return response()->json([
            'kos'   => $kos->nama_kos,
            'foto'  => Config::get('app.url') . '/' . $kos->foto,
            'kota'  => $kos->kota,
        ]);
    }

    public function uploadFotoUser(Request $request){
        $user = Auth::user();

        $request->validate([
            'foto'  => 'image',
        ]);

        if($request->foto == null){
            $user->update([
                'foto' => null,
            ]);

            return response()->json(['foto' => 'dihapus']);
        }

        $imageName = $user->id . time() . '.' . $request->file('foto')->getClientOriginalExtension();
        $path = $request->file('foto')->storeAs('public/foto_profil/', $imageName);
        $user->update([
            'foto'  => 'storage/foto_profil/' . $imageName,
        ]);

        return response()->json(['foto' => $imageName]);
    }

    public function getUserById($id)
    {
        $data_user = User::where('id', $id)->first();

        return fractal($data_user, new UserTransformer)->toArray();
    }

    public function profilProgressBar($id)
    {
        $user     = User::find($id);
        $counter  = 0;
        $total    = 0;
        $yang_harus_diisi = [];

        // Hitung yang kosong dari tabel user
        $user_array = fractal($user, new UserTransformer)->toArray();
        $key = ["foto", "nama_depan", "nama_belakang", "email", "jenis_kelamin", "tanggal_lahir", "kota_asal", "profesi_id", "telepon", "alamat"];
        for($i=0; $i<10; $i++){
            if($user_array['data'][$key[$i]] == null){
                $counter++;
                $yang_harus_diisi[] = $key[$i];
            }
            $total++;
        }

        // Hitung yang kosong jika profesi jenis_kuliah
        if($user->profesi_id == 1){
            $profesi_kuliah = ProfesiKuliah::where('user_id', $user->id)->first();
            $profesi_kuliah_array = fractal($profesi_kuliah, new ProfesiKuliahTransformer)->toArray();
            $key2 = ["jenis_kuliah", "universitas", "jurusan", "semester"];
            // dd($profesi_kuliah_array);
            for($i=0; $i<4; $i++){
                if($profesi_kuliah_array['data'][$key2[$i]] == null){
                    $counter++;
                    $yang_harus_diisi[] = $key2[$i];
                }
                $total++;
            }
            // Hitung yang kosong jika kuliah freelance
            if($profesi_kuliah->jenis_kuliah == 'freelance'){
                if($profesi_kuliah->profesi == null){
                    $counter++;
                    $yang_harus_diisi[] = "profesi";
                }
                $total++;
            }
            // Hitung yang kosong jika parttime
            if($profesi_kuliah->jenis_kuliah == 'parttime'){
                if($profesi_kuliah->profesi == null){
                    $counter++;
                    $yang_harus_diisi[] = "profesi";
                }
                if($profesi_kuliah->field == null){
                    $counter++;
                    $yang_harus_diisi[] = "field";
                }
                $total += 2;
            }
        }elseif ($user->profesi_id == 2) {  // Hitung yang kosong jika profesi bekerja
            $profesi_bekerja = ProfesiBekerja::where('user_id', $user->id)->first();
            if($profesi_bekerja->jenis_bekerja == null){
                $counter++;
                $total++;
                $yang_harus_diisi[] = "jenis_bekerja";
            }elseif ($profesi_bekerja->jenis_bekerja == 'onlineshop') {
                $total++;
            }else {
                $profesi_bekerja_array = fractal($profesi_bekerja, new ProfesiBekerjaTransformer)->toArray();
                $key3 = ["jenis_bekerja", "kantor", "posisi"];
                for($i=0; $i<3; $i++){
                    if($profesi_bekerja_array['data'][$key3[$i]] == null){
                        $counter++;
                        $yang_harus_diisi[] = $key3[$i];
                    }
                    $total++;
                }
            }
        }elseif ($user->profesi_id == 3) { // Hitung yang kosong jika profesi lainnya
            $profesi_lainnya = ProfesiLainnya::where('user_id', $user->id)->first();
            if(empty($profesi_lainnya->keterangan)){
                $counter++;
                $yang_harus_diisi[] = "keterangan";
            }
            $total++;
        }

        if($counter == 0){
            $persentase = 100;
        }else {
            $yang_sudah_diisi = $total - $counter;
            $hasil_perhitungan = ($yang_sudah_diisi / $total) * 100;
            $persentase = (int)$hasil_perhitungan;
        }

        return response()->json(['persentase' => $persentase]);
    }
}
