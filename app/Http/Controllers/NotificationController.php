<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Kos;
use App\Models\User;
use App\Transformers\NotificationTransformer;
use Auth;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = Notification::where('anak_kos_id', Auth::user()->id)->where('status', 1)->get();
        return fractal()
            ->collection($notifications)
            ->transformWith(new NotificationTransformer)
            ->toArray();
    }

    public function indexByOwner()
    {
        $user = Auth::user();
        $notification = Notification::where('anak_kos_id', null)->where('kos_id', $user->kos->id)->get();

        return fractal()
            ->collection($notification)
            ->transformWith(new NotificationTransformer)
            ->toArray();
    }

    public function notifikasiLengkapiData($id)
    {
        $user = Auth::user();
        $kos = Kos::where('user_id', Auth::user()->id)->first();
        Notification::create([
            'notification'  => 'Kos '.$kos->nama_kos.' meminta anda melengkapi profil anda.',
            'user_id'       => $user->id,
            'anak_kos_id'   => $id,
            'kos_id'        => $kos->id,
        ]);
        return response()->json(['message' => 'notifikasi dibuat']);
    }

    public function changeStatusToInactive($id)
    {
        $notification = Notification::find($id);
        $notification->status = 0;
        $notification->save();

        return response()->json(['message' => 'success']);
    }

    public function notifikasiKosBaru()
    {
        dd('Telah masuk ke fungsi notifikasi kos baru');
    }

    public function makeSurvey($id_kos)
    {
        $user = Auth::user();
        $kos = Kos::find($id_kos);

        if(empty($kos)){
            return response()->json(['error' => 'data kos tidak ditemukan'], 404);
        }

        $owner_kos = User::find($kos->user_id);

        Notification::create([
            'notification'  => $user->nama_depan . ' ingin melakukan survey ke kos anda',
            'user_id'       => $user->id,
            'kos_id'        => $kos->id,
        ]);

        return response()->json(['message' => 'notifikasi dibuat']);
    }
}
