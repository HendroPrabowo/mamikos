<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class OwnerKos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role != 1){
            return response()->json([
                'message' => 'akun anda bukan owner kos',
            ], 422);
        }
        return $next($request);
    }
}
