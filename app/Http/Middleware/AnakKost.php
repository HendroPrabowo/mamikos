<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AnakKost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role != 2){
            return response()->json([
                'message' => 'akun anda bukan anak kos',
            ], 422);
        }
        return $next($request);
    }
}
