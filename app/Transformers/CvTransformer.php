<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Cv;
use Config;

class CvTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Cv $cv)
    {
        if($cv->upload_cv == null){
            $upload_cv = null;
        }else {
            $upload_cv = Config::get('app.url') . '/' . $cv->upload_cv;
        }
        return [
            'id'                    => $cv->id,
            'nama_user'             => $cv->user->nama_depan,
            'user_id'               => $cv->user_id,
            'pendidikan_terakhir'   => $cv->pendidikan_terakhir,
            'penghasilan_terakhir'  => $cv->penghasilan_terakhir,
            'gaji_min'              => $cv->gaji_min,
            'gaji_max'              => $cv->gaji_max,
            'upload_cv'             => $upload_cv,
        ];
    }
}
