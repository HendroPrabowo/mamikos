<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;
use App\Models\ProfesiKuliah;
use App\Models\ProfesiBekerja;
use App\Models\ProfesiLainnya;
use Config;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        // Role
        $role = null;
        if($user->role == 1){
            $role = 'Owner Kos';
        }else{
            $role = 'Anak Kos';
        }

        $foto = null;
        if($user->foto == null){
            $foto = null;
        }else{
            $foto = Config::get('app.url') . '/' . $user->foto;
        }

        if($user->profesi_id == 1){
            $profesi_kuliah = ProfesiKuliah::where('user_id', $user->id)->first();
            return [
                'id'                  => $user->id,
                'foto'                => $foto,
                'nama_depan'          => $user->nama_depan,
                'nama_belakang'       => $user->nama_belakang,
                'email'               => $user->email,
                'jenis_kelamin'       => $user->jenis_kelamin,
                'tanggal_lahir'       => $user->tanggal_lahir,
                'kota_asal'           => $user->kota_asal,
                'profesi_id'          => $user->profesi_id,
                'jenis_kuliah'        => $profesi_kuliah->jenis_kuliah,
                'universitas'         => $profesi_kuliah->universitas,
                'jurusan'             => $profesi_kuliah->jurusan,
                'semester'            => $profesi_kuliah->semester,
                'profesi'             => $profesi_kuliah->profesi,
                'field'               => $profesi_kuliah->field,
                'telepon'             => $user->telepon,
                'alamat'              => $user->alamat,
                'role'                => $role,
                'api_token'           => $user->api_token,
            ];
        }elseif ($user->profesi_id == 2) {
            $profesi_bekerja = ProfesiBekerja::where('user_id', $user->id)->first();
            return [
                'id'                  => $user->id,
                'foto'                => $foto,
                'nama_depan'          => $user->nama_depan,
                'nama_belakang'       => $user->nama_belakang,
                'email'               => $user->email,
                'jenis_kelamin'       => $user->jenis_kelamin,
                'tanggal_lahir'       => $user->tanggal_lahir,
                'kota_asal'           => $user->kota_asal,
                'profesi_id'          => $user->profesi_id,
                'jenis_bekerja'       => $profesi_bekerja->jenis_bekerja,
                'kantor'              => $profesi_bekerja->kantor,
                'posisi'              => $profesi_bekerja->posisi,
                'telepon'             => $user->telepon,
                'alamat'              => $user->alamat,
                'role'                => $role,
                'api_token'           => $user->api_token,
            ];
        }elseif ($user->profesi_id == 3) {
            $profesi_lainnya = ProfesiLainnya::where('user_id', $user->id)->first();
            return [
                'id'                  => $user->id,
                'foto'                => $foto,
                'nama_depan'          => $user->nama_depan,
                'nama_belakang'       => $user->nama_belakang,
                'email'               => $user->email,
                'jenis_kelamin'       => $user->jenis_kelamin,
                'tanggal_lahir'       => $user->tanggal_lahir,
                'kota_asal'           => $user->kota_asal,
                'profesi_id'          => $user->profesi_id,
                'keterangan'          => $profesi_lainnya->keterangan,
                'telepon'             => $user->telepon,
                'alamat'              => $user->alamat,
                'role'                => $role,
                'api_token'           => $user->api_token,
            ];
        }
    }
}
