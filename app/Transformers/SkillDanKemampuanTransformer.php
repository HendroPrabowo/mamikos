<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SkillDanKemampuan;

class SkillDanKemampuanTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SkillDanKemampuan $skill)
    {
        return [
            'id'        => $skill->id,
            'cv_id'     => $skill->cv_id,
            'skill'     => $skill->skill,
        ];
    }
}
