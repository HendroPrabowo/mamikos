<?php

namespace App\Transformers;
use App\Models\PengalamanKerja;

use League\Fractal\TransformerAbstract;

class PengalamanKerjaTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PengalamanKerja $pengalaman_kerja)
    {
        return [
            'id'                => $pengalaman_kerja->id,
            'cv_id'             => $pengalaman_kerja->cv_id,
            'pengalaman_kerja'  => $pengalaman_kerja->pengalaman_kerja,
            'mulai'             => $pengalaman_kerja->mulai,
            'selesai'           => $pengalaman_kerja->selesai,
        ];
    }
}
