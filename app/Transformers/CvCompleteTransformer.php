<?php

namespace App\Transformers;

use App\Models\Cv;
use App\Models\User;
use App\Models\PengalamanKerja;
use App\Models\SkillDanKemampuan;
use App\Transformers\PengalamanKerjaTransformer;
use App\Transformers\SkillDanKemampuanTransformer;
use League\Fractal\TransformerAbstract;
use Config;

class CvCompleteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Cv $cv)
    {
        $user = User::find($cv->user->id);

        // Data pengalaman kerja
        $penglaman_kerja = PengalamanKerja::where('cv_id', $cv->id)->get();
        $penglaman_kerja_array = fractal()->collection($penglaman_kerja)->transformWith(new PengalamanKerjaTransformer)->toArray();

        // Data skill dan kemampuan
        $skill_dan_kemampuan = SkillDanKemampuan::where('cv_id', $cv->id)->get();
        $skill_dan_kemampuan_array = fractal()->collection($skill_dan_kemampuan)->transformWith(new SkillDanKemampuanTransformer)->toArray();

        if($cv->upload_cv == null){
            $upload_cv = null;
        }else {
            $upload_cv = Config::get('app.url') . '/' . $cv->upload_cv;
        }

        return [
            'id'                    => $cv->id,
            'nama_user'             => $cv->user->nama_depan,
            'user_id'               => $cv->user_id,
            'pendidikan_terakhir'   => $cv->pendidikan_terakhir,
            'penghasilan_terakhir'  => $cv->penghasilan_terakhir,
            'gaji_max'              => $cv->gaji_max,
            'gaji_min'              => $cv->gaji_min,
            'upload_cv'             => $upload_cv,
            'pengalaman_kerja'      => $penglaman_kerja_array,
            'skill_dan_kemampuan'   => $skill_dan_kemampuan_array,
        ];
    }
}
