<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ProfesiBekerja;

class ProfesiBekerjaTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProfesiBekerja $profesi_bekerja)
    {
        return [
            'id'            => $profesi_bekerja->id,
            'user_id'       => $profesi_bekerja->user_id,
            'jenis_bekerja' => $profesi_bekerja->jenis_bekerja,
            'kantor'        => $profesi_bekerja->kantor,
            'posisi'        => $profesi_bekerja->posisi,
        ];
    }
}
