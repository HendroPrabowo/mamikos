<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ProfesiKuliah;

class ProfesiKuliahTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProfesiKuliah $profesi_kuliah)
    {
        return [
            'id'            => $profesi_kuliah->id,
            'user_id'       => $profesi_kuliah->user_id,
            'jenis_kuliah'  => $profesi_kuliah->jenis_kuliah,
            'universitas'   => $profesi_kuliah->universitas,
            'jurusan'       => $profesi_kuliah->jurusan,
            'semester'      => $profesi_kuliah->semester,
            'profesi'       => $profesi_kuliah->profesi,
            'field'         => $profesi_kuliah->field,
        ];
    }
}
