<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Notification;
use Config;

class NotificationTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Notification $notification)
    {
        if(empty($notification->user->foto)){
            $foto_user = null;
        }else {
            $foto_user = Config::get('app.url') . '/' . $notification->user->foto;
        }
        return [
            'id'              => $notification->id,
            'user_id'         => $notification->user_id,
            'anak_kos_id'     => $notification->anak_kos_id,
            'kos_id'          => $notification->kos_id,
            'foto_kos'        => Config::get('app.url') . '/' . $notification->kos->foto,
            'foto_anak_kos'   => $foto_user,
            'status'          => $notification->status,
            'notification'    => $notification->notification,
        ];
    }
}
