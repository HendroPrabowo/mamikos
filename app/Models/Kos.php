<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kos extends Model
{
    use softDeletes;

    protected $table    = 'kos';
    protected $guarded  = ['id'];
    protected $dates    = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
