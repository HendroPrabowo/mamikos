<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    protected $table    = 'notification';
    protected $guarded  = ['id'];
    protected $dates    = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function kos()
    {
        return $this->belongsTo('App\Models\Kos');
    }
}
