<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded  = ['id'];
    protected $dates    = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cv()
    {
        return $this->hasOne('App\Models\Cv');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification');
    }

    public function kos()
    {
        return $this->hasOne('App\Models\Kos');
    }
}
