<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfesiLainnya extends Model
{
    use softDeletes;

    protected $table    = 'profesi_lainnya';
    protected $guarded  = ['id'];
    protected $dates    = ['deleted_at'];
}
