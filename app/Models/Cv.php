<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cv extends Model
{
    use softDeletes;

    protected $table    = 'cv';
    protected $guarded  = ['id'];
    protected $dates    = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function skills()
    {
        return $this->hasMany('App\Models\SkillDanKemampuan');
    }

    public function pengalamanKerja()
    {
        return $this->hasMany('App\Models\PengalamanKerja');
    }
}
