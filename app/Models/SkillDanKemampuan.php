<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkillDanKemampuan extends Model
{
    use softDeletes;

    protected $table      = 'skill_dan_kemampuan';
    protected $guarded    = ['id'];
    protected $dates      = ['deleted_at'];

    public function cv()
    {
        return $this->belongsTo('App\Models\Cv');
    }
}
