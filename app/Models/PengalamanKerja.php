<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengalamanKerja extends Model
{
    use softDeletes;

    protected $table    = 'pengalaman_kerja';
    protected $guarded  = ['id'];
    protected $dates    = ['deleted_at'];

    public function cv()
    {
        return $this->belongsTo('App\Models\Cv');
    }
}
