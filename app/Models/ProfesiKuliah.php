<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfesiKuliah extends Model
{
    use softDeletes;

    protected $table    = 'profesi_kuliah';
    protected $guarded  = ['id'];
    protected $dates    = ['deleted_at'];
}
