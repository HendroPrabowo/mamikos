<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto', 300)->nullable();
            $table->string('nama_depan', 300);
            $table->string('nama_belakang', 300);
            $table->string('email')->unique();
            $table->enum('jenis_kelamin', ['L', 'P'])->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('kota_asal', 300)->nullable();
            $table->unsignedInteger('profesi_id');
            $table->string('telepon', 20);
            $table->string('alamat', 500)->nullable();
            $table->unsignedInteger('kost_saya')->nullable();
            $table->string('password');
            $table->string('api_token');
            $table->tinyInteger('role')->default(2);
            $table->rememberToken();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
