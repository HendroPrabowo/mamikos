<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesiKuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesi_kuliah', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->enum('jenis_kuliah', ['kuliahsaja', 'freelance', 'parttime', 'onlineshop'])->nullable();
            $table->string('universitas', 200)->nullable();
            $table->string('jurusan', 200)->nullable();
            $table->string('semester', 100)->nullable();
            $table->string('profesi', 200)->nullable();
            $table->string('field', 200)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesi_kuliah');
    }
}
